using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace EmailSendTest01
{
    public class Tests
    {
        private IWebDriver _driver;

        private readonly By _loginField = By.ClassName("_2yPTK9xQ");
        private readonly By _passwordField = By.Name("password");
        private readonly By _continueButton = By.CssSelector(".Ol0-ktls");
        private readonly By _composeButton = By.XPath("//button[@class='button primary compose']");
        private readonly By _sendmsgTo = By.Name("toFieldInput");
        private readonly By _sendmsgSubject = By.Name("subject");
        private readonly By _messageBody = By.Id("mce_0_ifr");
        private readonly By _sending = By.XPath("//button[@class='button primary send']");
        private readonly By _confirmSending = By.ClassName("sendmsg__ads-ready");

        private readonly string user1email = "testuser01@ukr.net";
        private readonly string user2login = "testuser02";
        private readonly string user2password = "********"; //hidden

        private bool MessageSent()
        {
            try
            {
                _driver.FindElement(_confirmSending);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Navigate().GoToUrl("https://accounts.ukr.net/login?lang=en");
            _driver.Manage().Window.Maximize();
        }

        [Test]
        public void Test1()
        {
            var SearchLoginField = _driver.FindElement(_loginField);
            SearchLoginField.SendKeys(user2login);

            var SearchPasswordField = _driver.FindElement(_passwordField);
            SearchPasswordField.SendKeys(user2password);
            
            WebDriverWait waitForSearchInput = new WebDriverWait(_driver, TimeSpan.FromSeconds(2));
            waitForSearchInput.Until(ExpectedConditions.ElementToBeClickable(_continueButton));
            var SearchButtonPressing = _driver.FindElement(_continueButton);
            SearchButtonPressing.Submit();

            WebDriverWait waitForLogin = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            waitForLogin.Until(ExpectedConditions.ElementToBeClickable(_composeButton));
            var Compose = _driver.FindElement(_composeButton);
            Compose.Click();

            var SendMessageFormTitle = _driver.FindElement(_sendmsgTo);
            SendMessageFormTitle.SendKeys(user1email);

            var SendMessageFormSubject = _driver.FindElement(_sendmsgSubject);
            SendMessageFormSubject.SendKeys("test_message");

            WebDriverWait waitMessageText = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            waitMessageText.Until(ExpectedConditions.ElementToBeClickable(_messageBody));
            var SendMessageContent = _driver.FindElement(_messageBody);
            SendMessageContent.SendKeys("Lorem ipsum dolor sit amet");

            var Send = _driver.FindElement(_sending);
            Send.Click();

            WebDriverWait waitSendingConfirmed = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            waitSendingConfirmed.Until(ExpectedConditions.ElementExists(_confirmSending));
            
            Assert.That(MessageSent());
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Quit();
        }
    }
}
