## Selenium

### ImageSearchTest01
Create a single test using WebDriver: 
- Navigate to the Google search page, 
- Search for an image,  
- Verify that an image tab contains images.

### EmailSendTest01
Send an email
- Log in to an email website
- Create a simple mail
- Send this mail to another test user
- Use at least 4 types of locators on the home task.